using System.ComponentModel;

namespace RainbowCaster {
    partial class AboutForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.InfoTextLabel = new System.Windows.Forms.Label();
            this.OKButton = new System.Windows.Forms.Button();
            this.GitlabLinkLabel = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // InfoTextLabel
            // 
            this.InfoTextLabel.Location = new System.Drawing.Point(10, 8);
            this.InfoTextLabel.Name = "InfoTextLabel";
            this.InfoTextLabel.Size = new System.Drawing.Size(394, 291);
            this.InfoTextLabel.TabIndex = 0;
            this.InfoTextLabel.Text = "About...";
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(12, 340);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(390, 31);
            this.OKButton.TabIndex = 2;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // GitlabLinkLabel
            // 
            this.GitlabLinkLabel.Location = new System.Drawing.Point(10, 312);
            this.GitlabLinkLabel.Name = "GitlabLinkLabel";
            this.GitlabLinkLabel.Size = new System.Drawing.Size(391, 13);
            this.GitlabLinkLabel.TabIndex = 3;
            this.GitlabLinkLabel.TabStop = true;
            this.GitlabLinkLabel.Text = "https://gitlab.com/sthorsten/rainbow-caster";
            this.GitlabLinkLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.GitlabLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.GitlabLinkLabel_LinkClicked);
            // 
            // AboutForm
            // 
            this.AcceptButton = this.OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 383);
            this.ControlBox = false;
            this.Controls.Add(this.GitlabLinkLabel);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.InfoTextLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AboutForm";
            this.Load += new System.EventHandler(this.AboutForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label InfoTextLabel;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.LinkLabel GitlabLinkLabel;
    }
}