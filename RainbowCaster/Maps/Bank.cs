﻿namespace RainbowCaster.Maps {
    internal class Bank : BombSpots {
        private const string ExecutiveCEO = "2F - Executive Lounge / CEO Office";
        private const string LockersCCTV = "B - Lockers / CCTV Room";
        private const string StaffOpenArea = "1F - Staff Room / Open Area";
        private const string TellersArchives = "1F - Tellers Office / Archives";

        override
            public string[] GetAsArray() {
            return new[] {LockersCCTV, StaffOpenArea, TellersArchives, ExecutiveCEO};
        }
    }
}