﻿namespace RainbowCaster.Maps {
    internal class Oregon : BombSpots {
        private const string KidsDorms = "2F - Kids Dorms / Dorms Main Hall";
        private const string KitchenDining = "1F - Kitchen / Dining Hall";
        private const string LaundrySupply = "B - Laundry Room / Supply Room";
        private const string TowerStage = "1F / 2F - Rear Stage / Watch Tower";

        override
            public string[] GetAsArray() {
            return new[] {LaundrySupply, KitchenDining, TowerStage, KidsDorms};
        }
    }
}