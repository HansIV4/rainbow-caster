namespace RainbowCaster.Maps
{
    public class MapPool
    {
        public const string Bank = "Bank";
        public const string Border = "Border";
        public const string Clubhouse = "Clubhouse";
        public const string Coastline = "Coastline";
        public const string Consulate = "Consulate";
        public const string Kafe = "Kafe Dostoyevsky";
        public const string Oregon = "Oregon";
        public const string Villa = "Villa";

        public static string[] GetMapPool()
        {
            return new[] { Bank, Border, Clubhouse, Coastline, Consulate, Kafe, Oregon, Villa };
        }
    }
}