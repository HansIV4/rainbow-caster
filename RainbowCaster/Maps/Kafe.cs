﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RainbowCaster.Maps
{
    internal class Kafe : BombSpots
    {
        private const string CocktailLounge = "3F - Cocktail Lounge";
        private const string FireplaceMining = "2F - Fireplace Hall / Mining Room";
        private const string ReadingFireplace = "2F - Reading Room / Fireplace Hall";
        private const string KitchenServiceCooking = "1F -  Kitchen Service / Kitchen Cooking";

        override
            public string[] GetAsArray()
        {
            return new[] { CocktailLounge, FireplaceMining, ReadingFireplace, KitchenServiceCooking };
        }
    }
}
