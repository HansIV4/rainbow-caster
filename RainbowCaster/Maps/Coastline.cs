﻿namespace RainbowCaster.Maps {
    internal class Coastline : BombSpots {
        private const string DoubleBars = "1F - Blue Bar / Sunrise Bar";
        private const string HookahBilliards = "2F - Hookah Lounge / Billiards Room";
        private const string KitchenService = "1F - Kitchen / Service Entrance";
        private const string PenthouseTheater = "2F - Penthouse / Theater";

        override
            public string[] GetAsArray() {
            return new[] {DoubleBars, KitchenService, HookahBilliards, PenthouseTheater};
        }
    }
}