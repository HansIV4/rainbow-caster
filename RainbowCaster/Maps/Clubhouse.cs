﻿namespace RainbowCaster.Maps {
    internal class Clubhouse : BombSpots {
        private const string BarStock = "1F - Bar / Stock Room";
        private const string BedroomGym = "2F - Bedroom / Gym";
        private const string CashCCTV = "2F - Cash Room / CCTV Room";
        private const string ChurchArsenal = "B - Church / Arsenal Room";

        override
            public string[] GetAsArray() {
            return new[] {ChurchArsenal, BarStock, BedroomGym, CashCCTV};
        }
    }
}