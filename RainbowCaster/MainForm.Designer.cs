﻿namespace RainbowCaster {
    partial class MainForm {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.TeamOrangeTextBox = new System.Windows.Forms.TextBox();
            this.TeamBlueTextBox = new System.Windows.Forms.TextBox();
            this.TeamBlueLabel = new System.Windows.Forms.Label();
            this.TeamOrangeLabel = new System.Windows.Forms.Label();
            this.QuitButton = new System.Windows.Forms.Button();
            this.TeamInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.MenuStrip = new System.Windows.Forms.MenuStrip();
            this.FileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.NewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExportMatchToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ExportMarkdownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExportCSVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SaveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SaveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.QuitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EditMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.ResetMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ResetMatchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HelpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.AboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.UpdateCheckToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.languageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.englishDefaultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.germanDeutschToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MatchInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.MatchURLTextBox = new System.Windows.Forms.TextBox();
            this.MatchURLLabel = new System.Windows.Forms.Label();
            this.MatchTitleTextBox = new System.Windows.Forms.TextBox();
            this.MatchTitleLabel = new System.Windows.Forms.Label();
            this.AddRoundGroupBox = new System.Windows.Forms.GroupBox();
            this.RoundAddPanel = new System.Windows.Forms.TableLayoutPanel();
            this.RoundAddControlPanel1 = new System.Windows.Forms.Panel();
            this.RoundBombSpotComboBox = new System.Windows.Forms.ComboBox();
            this.RoundBombSpotLabel = new System.Windows.Forms.Label();
            this.RoundPlantLabel = new System.Windows.Forms.Label();
            this.PlantRadioButtonPanel = new System.Windows.Forms.Panel();
            this.RoundPlantYesRadioButton = new System.Windows.Forms.RadioButton();
            this.RoundPlantNoRadioButton = new System.Windows.Forms.RadioButton();
            this.RoundWinLabel = new System.Windows.Forms.Label();
            this.RoundPlantDeniedLabel = new System.Windows.Forms.Label();
            this.RoundPlantDeniedNoRadioButton = new System.Windows.Forms.RadioButton();
            this.RoundPlantDeniedYesRadioButton = new System.Windows.Forms.RadioButton();
            this.RoundWinComboBox = new System.Windows.Forms.ComboBox();
            this.RoundAddControlPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.RemoveRoundButton = new System.Windows.Forms.Button();
            this.AddRoundButton = new System.Windows.Forms.Button();
            this.RoundAddControlPanel2 = new System.Windows.Forms.Panel();
            this.RoundExtraInfoCheckBoxList = new System.Windows.Forms.CheckedListBox();
            this.RoundExtraInfoLabel = new System.Windows.Forms.Label();
            this.RoundListView = new System.Windows.Forms.ListView();
            this.Round = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.BombSpot = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Plant = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CounterDefuse = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Extra = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Win = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Score = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CopyrightLabel = new System.Windows.Forms.Label();
            this.MainTabs = new System.Windows.Forms.TabControl();
            this.MatchDataTab = new System.Windows.Forms.TabPage();
            this.MatchDataPanel = new System.Windows.Forms.TableLayoutPanel();
            this.MapInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.Map3ComboBox = new System.Windows.Forms.ComboBox();
            this.Map2ComboBox = new System.Windows.Forms.ComboBox();
            this.Map3Label = new System.Windows.Forms.Label();
            this.Map2Label = new System.Windows.Forms.Label();
            this.Map1ComboBox = new System.Windows.Forms.ComboBox();
            this.Map1Label = new System.Windows.Forms.Label();
            this.SpielModusComboBox = new System.Windows.Forms.ComboBox();
            this.GameModeLabel = new System.Windows.Forms.Label();
            this.ExtendedMatchInfoGroup = new System.Windows.Forms.GroupBox();
            this.ExtendedMatchInfoTextBox = new System.Windows.Forms.TextBox();
            this.MatchConfirmButton = new System.Windows.Forms.Button();
            this.CastersNotesGroup = new System.Windows.Forms.GroupBox();
            this.CastersNotesMatchDataTextBox = new System.Windows.Forms.TextBox();
            this.OperatorBanTab = new System.Windows.Forms.TabPage();
            this.OperatorBanPanel = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CastersNotesOperatorBansTextBox = new System.Windows.Forms.TextBox();
            this.BlueBanGroup = new System.Windows.Forms.GroupBox();
            this.DEFBanLabel1 = new System.Windows.Forms.Label();
            this.ATKBanLabel1 = new System.Windows.Forms.Label();
            this.TeamBlauATKBanComboBox = new System.Windows.Forms.ComboBox();
            this.TeamBlauDEFBanComboBox = new System.Windows.Forms.ComboBox();
            this.OrangeBanGroup = new System.Windows.Forms.GroupBox();
            this.DEFBanLabel2 = new System.Windows.Forms.Label();
            this.ATKBanLabel2 = new System.Windows.Forms.Label();
            this.TeamOrangeDEFBanComboBox = new System.Windows.Forms.ComboBox();
            this.TeamOrangeATKBanComboBox = new System.Windows.Forms.ComboBox();
            this.BanConfirmButton = new System.Windows.Forms.Button();
            this.MapDataTab = new System.Windows.Forms.TabPage();
            this.RoundPanel = new System.Windows.Forms.TableLayoutPanel();
            this.CastersNotesRoundsGroup = new System.Windows.Forms.GroupBox();
            this.CastersNotesRoundsTextBox = new System.Windows.Forms.TextBox();
            this.EndMapButton = new System.Windows.Forms.Button();
            this.MainLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.TeamInfoGroupBox.SuspendLayout();
            this.MenuStrip.SuspendLayout();
            this.MatchInfoGroupBox.SuspendLayout();
            this.AddRoundGroupBox.SuspendLayout();
            this.RoundAddPanel.SuspendLayout();
            this.RoundAddControlPanel1.SuspendLayout();
            this.PlantRadioButtonPanel.SuspendLayout();
            this.RoundAddControlPanel3.SuspendLayout();
            this.RoundAddControlPanel2.SuspendLayout();
            this.MainTabs.SuspendLayout();
            this.MatchDataTab.SuspendLayout();
            this.MatchDataPanel.SuspendLayout();
            this.MapInfoGroupBox.SuspendLayout();
            this.ExtendedMatchInfoGroup.SuspendLayout();
            this.CastersNotesGroup.SuspendLayout();
            this.OperatorBanTab.SuspendLayout();
            this.OperatorBanPanel.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.BlueBanGroup.SuspendLayout();
            this.OrangeBanGroup.SuspendLayout();
            this.MapDataTab.SuspendLayout();
            this.RoundPanel.SuspendLayout();
            this.CastersNotesRoundsGroup.SuspendLayout();
            this.MainLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // TeamOrangeTextBox
            // 
            resources.ApplyResources(this.TeamOrangeTextBox, "TeamOrangeTextBox");
            this.TeamOrangeTextBox.Name = "TeamOrangeTextBox";
            // 
            // TeamBlueTextBox
            // 
            resources.ApplyResources(this.TeamBlueTextBox, "TeamBlueTextBox");
            this.TeamBlueTextBox.Name = "TeamBlueTextBox";
            // 
            // TeamBlueLabel
            // 
            resources.ApplyResources(this.TeamBlueLabel, "TeamBlueLabel");
            this.TeamBlueLabel.Name = "TeamBlueLabel";
            // 
            // TeamOrangeLabel
            // 
            resources.ApplyResources(this.TeamOrangeLabel, "TeamOrangeLabel");
            this.TeamOrangeLabel.Name = "TeamOrangeLabel";
            // 
            // QuitButton
            // 
            resources.ApplyResources(this.QuitButton, "QuitButton");
            this.QuitButton.Name = "QuitButton";
            this.QuitButton.UseVisualStyleBackColor = true;
            this.QuitButton.Click += new System.EventHandler(this.QuitButton_Click);
            // 
            // TeamInfoGroupBox
            // 
            resources.ApplyResources(this.TeamInfoGroupBox, "TeamInfoGroupBox");
            this.TeamInfoGroupBox.Controls.Add(this.TeamOrangeTextBox);
            this.TeamInfoGroupBox.Controls.Add(this.TeamOrangeLabel);
            this.TeamInfoGroupBox.Controls.Add(this.TeamBlueTextBox);
            this.TeamInfoGroupBox.Controls.Add(this.TeamBlueLabel);
            this.TeamInfoGroupBox.Name = "TeamInfoGroupBox";
            this.TeamInfoGroupBox.TabStop = false;
            // 
            // MenuStrip
            // 
            this.MenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileMenu,
            this.EditMenu,
            this.HelpMenu,
            this.languageToolStripMenuItem});
            resources.ApplyResources(this.MenuStrip, "MenuStrip");
            this.MenuStrip.Name = "MenuStrip";
            // 
            // FileMenu
            // 
            this.FileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewToolStripMenuItem,
            this.OpenToolStripMenuItem,
            this.ExportMatchToolStripMenuItem1,
            this.SaveToolStripMenuItem,
            this.SaveAsToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.QuitToolStripMenuItem});
            this.FileMenu.Name = "FileMenu";
            resources.ApplyResources(this.FileMenu, "FileMenu");
            // 
            // NewToolStripMenuItem
            // 
            resources.ApplyResources(this.NewToolStripMenuItem, "NewToolStripMenuItem");
            this.NewToolStripMenuItem.Name = "NewToolStripMenuItem";
            // 
            // OpenToolStripMenuItem
            // 
            resources.ApplyResources(this.OpenToolStripMenuItem, "OpenToolStripMenuItem");
            this.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem";
            // 
            // ExportMatchToolStripMenuItem1
            // 
            this.ExportMatchToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExportMarkdownToolStripMenuItem,
            this.ExportCSVToolStripMenuItem});
            this.ExportMatchToolStripMenuItem1.Name = "ExportMatchToolStripMenuItem1";
            resources.ApplyResources(this.ExportMatchToolStripMenuItem1, "ExportMatchToolStripMenuItem1");
            // 
            // ExportMarkdownToolStripMenuItem
            // 
            this.ExportMarkdownToolStripMenuItem.Name = "ExportMarkdownToolStripMenuItem";
            resources.ApplyResources(this.ExportMarkdownToolStripMenuItem, "ExportMarkdownToolStripMenuItem");
            this.ExportMarkdownToolStripMenuItem.Click += new System.EventHandler(this.AlsMarkdownToolStripMenuItem_Click);
            // 
            // ExportCSVToolStripMenuItem
            // 
            this.ExportCSVToolStripMenuItem.Name = "ExportCSVToolStripMenuItem";
            resources.ApplyResources(this.ExportCSVToolStripMenuItem, "ExportCSVToolStripMenuItem");
            this.ExportCSVToolStripMenuItem.Click += new System.EventHandler(this.AlsCSVToolStripMenuItem_Click);
            // 
            // SaveToolStripMenuItem
            // 
            resources.ApplyResources(this.SaveToolStripMenuItem, "SaveToolStripMenuItem");
            this.SaveToolStripMenuItem.Name = "SaveToolStripMenuItem";
            // 
            // SaveAsToolStripMenuItem
            // 
            resources.ApplyResources(this.SaveAsToolStripMenuItem, "SaveAsToolStripMenuItem");
            this.SaveAsToolStripMenuItem.Name = "SaveAsToolStripMenuItem";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            resources.ApplyResources(this.settingsToolStripMenuItem, "settingsToolStripMenuItem");
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.SettingsToolStripMenuItem_Click);
            // 
            // QuitToolStripMenuItem
            // 
            this.QuitToolStripMenuItem.Name = "QuitToolStripMenuItem";
            resources.ApplyResources(this.QuitToolStripMenuItem, "QuitToolStripMenuItem");
            this.QuitToolStripMenuItem.Click += new System.EventHandler(this.beendenToolStripMenuItem_Click);
            // 
            // EditMenu
            // 
            this.EditMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ResetMapToolStripMenuItem,
            this.ResetMatchToolStripMenuItem});
            this.EditMenu.Name = "EditMenu";
            resources.ApplyResources(this.EditMenu, "EditMenu");
            // 
            // ResetMapToolStripMenuItem
            // 
            this.ResetMapToolStripMenuItem.Name = "ResetMapToolStripMenuItem";
            resources.ApplyResources(this.ResetMapToolStripMenuItem, "ResetMapToolStripMenuItem");
            this.ResetMapToolStripMenuItem.Click += new System.EventHandler(this.ResetMapToolStripMenuItem_Click);
            // 
            // ResetMatchToolStripMenuItem
            // 
            resources.ApplyResources(this.ResetMatchToolStripMenuItem, "ResetMatchToolStripMenuItem");
            this.ResetMatchToolStripMenuItem.Name = "ResetMatchToolStripMenuItem";
            // 
            // HelpMenu
            // 
            this.HelpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AboutToolStripMenuItem,
            this.UpdateCheckToolStripMenuItem});
            this.HelpMenu.Name = "HelpMenu";
            resources.ApplyResources(this.HelpMenu, "HelpMenu");
            // 
            // AboutToolStripMenuItem
            // 
            this.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem";
            resources.ApplyResources(this.AboutToolStripMenuItem, "AboutToolStripMenuItem");
            this.AboutToolStripMenuItem.Click += new System.EventHandler(this.überDasProgrammToolStripMenuItem_Click);
            // 
            // UpdateCheckToolStripMenuItem
            // 
            this.UpdateCheckToolStripMenuItem.Name = "UpdateCheckToolStripMenuItem";
            resources.ApplyResources(this.UpdateCheckToolStripMenuItem, "UpdateCheckToolStripMenuItem");
            this.UpdateCheckToolStripMenuItem.Click += new System.EventHandler(this.AufUpdatesÜberprüfenToolStripMenuItem_Click);
            // 
            // languageToolStripMenuItem
            // 
            this.languageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.englishDefaultToolStripMenuItem,
            this.germanDeutschToolStripMenuItem});
            this.languageToolStripMenuItem.Name = "languageToolStripMenuItem";
            resources.ApplyResources(this.languageToolStripMenuItem, "languageToolStripMenuItem");
            // 
            // englishDefaultToolStripMenuItem
            // 
            this.englishDefaultToolStripMenuItem.Name = "englishDefaultToolStripMenuItem";
            resources.ApplyResources(this.englishDefaultToolStripMenuItem, "englishDefaultToolStripMenuItem");
            this.englishDefaultToolStripMenuItem.Click += new System.EventHandler(this.EnglishDefaultToolStripMenuItem_Click);
            // 
            // germanDeutschToolStripMenuItem
            // 
            this.germanDeutschToolStripMenuItem.Name = "germanDeutschToolStripMenuItem";
            resources.ApplyResources(this.germanDeutschToolStripMenuItem, "germanDeutschToolStripMenuItem");
            this.germanDeutschToolStripMenuItem.Click += new System.EventHandler(this.GermanDeutschToolStripMenuItem_Click);
            // 
            // MatchInfoGroupBox
            // 
            resources.ApplyResources(this.MatchInfoGroupBox, "MatchInfoGroupBox");
            this.MatchInfoGroupBox.Controls.Add(this.MatchURLTextBox);
            this.MatchInfoGroupBox.Controls.Add(this.MatchURLLabel);
            this.MatchInfoGroupBox.Controls.Add(this.MatchTitleTextBox);
            this.MatchInfoGroupBox.Controls.Add(this.MatchTitleLabel);
            this.MatchInfoGroupBox.Name = "MatchInfoGroupBox";
            this.MatchInfoGroupBox.TabStop = false;
            // 
            // MatchURLTextBox
            // 
            this.MatchURLTextBox.AcceptsReturn = true;
            resources.ApplyResources(this.MatchURLTextBox, "MatchURLTextBox");
            this.MatchURLTextBox.Name = "MatchURLTextBox";
            // 
            // MatchURLLabel
            // 
            resources.ApplyResources(this.MatchURLLabel, "MatchURLLabel");
            this.MatchURLLabel.Name = "MatchURLLabel";
            // 
            // MatchTitleTextBox
            // 
            resources.ApplyResources(this.MatchTitleTextBox, "MatchTitleTextBox");
            this.MatchTitleTextBox.Name = "MatchTitleTextBox";
            this.MatchTitleTextBox.TextChanged += new System.EventHandler(this.MatchTitleTextBox_TextChanged);
            // 
            // MatchTitleLabel
            // 
            resources.ApplyResources(this.MatchTitleLabel, "MatchTitleLabel");
            this.MatchTitleLabel.Name = "MatchTitleLabel";
            // 
            // AddRoundGroupBox
            // 
            this.AddRoundGroupBox.Controls.Add(this.RoundAddPanel);
            resources.ApplyResources(this.AddRoundGroupBox, "AddRoundGroupBox");
            this.AddRoundGroupBox.Name = "AddRoundGroupBox";
            this.AddRoundGroupBox.TabStop = false;
            // 
            // RoundAddPanel
            // 
            resources.ApplyResources(this.RoundAddPanel, "RoundAddPanel");
            this.RoundAddPanel.Controls.Add(this.RoundAddControlPanel1, 0, 0);
            this.RoundAddPanel.Controls.Add(this.RoundAddControlPanel3, 1, 1);
            this.RoundAddPanel.Controls.Add(this.RoundAddControlPanel2, 1, 0);
            this.RoundAddPanel.Name = "RoundAddPanel";
            // 
            // RoundAddControlPanel1
            // 
            this.RoundAddControlPanel1.Controls.Add(this.RoundBombSpotComboBox);
            this.RoundAddControlPanel1.Controls.Add(this.RoundBombSpotLabel);
            this.RoundAddControlPanel1.Controls.Add(this.RoundPlantLabel);
            this.RoundAddControlPanel1.Controls.Add(this.PlantRadioButtonPanel);
            this.RoundAddControlPanel1.Controls.Add(this.RoundWinLabel);
            this.RoundAddControlPanel1.Controls.Add(this.RoundPlantDeniedLabel);
            this.RoundAddControlPanel1.Controls.Add(this.RoundPlantDeniedNoRadioButton);
            this.RoundAddControlPanel1.Controls.Add(this.RoundPlantDeniedYesRadioButton);
            this.RoundAddControlPanel1.Controls.Add(this.RoundWinComboBox);
            resources.ApplyResources(this.RoundAddControlPanel1, "RoundAddControlPanel1");
            this.RoundAddControlPanel1.Name = "RoundAddControlPanel1";
            this.RoundAddPanel.SetRowSpan(this.RoundAddControlPanel1, 2);
            // 
            // RoundBombSpotComboBox
            // 
            resources.ApplyResources(this.RoundBombSpotComboBox, "RoundBombSpotComboBox");
            this.RoundBombSpotComboBox.FormattingEnabled = true;
            this.RoundBombSpotComboBox.Name = "RoundBombSpotComboBox";
            // 
            // RoundBombSpotLabel
            // 
            resources.ApplyResources(this.RoundBombSpotLabel, "RoundBombSpotLabel");
            this.RoundBombSpotLabel.Name = "RoundBombSpotLabel";
            // 
            // RoundPlantLabel
            // 
            resources.ApplyResources(this.RoundPlantLabel, "RoundPlantLabel");
            this.RoundPlantLabel.Name = "RoundPlantLabel";
            // 
            // PlantRadioButtonPanel
            // 
            this.PlantRadioButtonPanel.Controls.Add(this.RoundPlantYesRadioButton);
            this.PlantRadioButtonPanel.Controls.Add(this.RoundPlantNoRadioButton);
            resources.ApplyResources(this.PlantRadioButtonPanel, "PlantRadioButtonPanel");
            this.PlantRadioButtonPanel.Name = "PlantRadioButtonPanel";
            // 
            // RoundPlantYesRadioButton
            // 
            resources.ApplyResources(this.RoundPlantYesRadioButton, "RoundPlantYesRadioButton");
            this.RoundPlantYesRadioButton.Name = "RoundPlantYesRadioButton";
            this.RoundPlantYesRadioButton.TabStop = true;
            this.RoundPlantYesRadioButton.UseVisualStyleBackColor = true;
            // 
            // RoundPlantNoRadioButton
            // 
            resources.ApplyResources(this.RoundPlantNoRadioButton, "RoundPlantNoRadioButton");
            this.RoundPlantNoRadioButton.Checked = true;
            this.RoundPlantNoRadioButton.Name = "RoundPlantNoRadioButton";
            this.RoundPlantNoRadioButton.TabStop = true;
            this.RoundPlantNoRadioButton.UseVisualStyleBackColor = true;
            // 
            // RoundWinLabel
            // 
            resources.ApplyResources(this.RoundWinLabel, "RoundWinLabel");
            this.RoundWinLabel.Name = "RoundWinLabel";
            // 
            // RoundPlantDeniedLabel
            // 
            resources.ApplyResources(this.RoundPlantDeniedLabel, "RoundPlantDeniedLabel");
            this.RoundPlantDeniedLabel.Name = "RoundPlantDeniedLabel";
            // 
            // RoundPlantDeniedNoRadioButton
            // 
            resources.ApplyResources(this.RoundPlantDeniedNoRadioButton, "RoundPlantDeniedNoRadioButton");
            this.RoundPlantDeniedNoRadioButton.Checked = true;
            this.RoundPlantDeniedNoRadioButton.Name = "RoundPlantDeniedNoRadioButton";
            this.RoundPlantDeniedNoRadioButton.TabStop = true;
            this.RoundPlantDeniedNoRadioButton.UseVisualStyleBackColor = true;
            // 
            // RoundPlantDeniedYesRadioButton
            // 
            resources.ApplyResources(this.RoundPlantDeniedYesRadioButton, "RoundPlantDeniedYesRadioButton");
            this.RoundPlantDeniedYesRadioButton.Name = "RoundPlantDeniedYesRadioButton";
            this.RoundPlantDeniedYesRadioButton.TabStop = true;
            this.RoundPlantDeniedYesRadioButton.UseVisualStyleBackColor = true;
            // 
            // RoundWinComboBox
            // 
            resources.ApplyResources(this.RoundWinComboBox, "RoundWinComboBox");
            this.RoundWinComboBox.FormattingEnabled = true;
            this.RoundWinComboBox.Name = "RoundWinComboBox";
            // 
            // RoundAddControlPanel3
            // 
            resources.ApplyResources(this.RoundAddControlPanel3, "RoundAddControlPanel3");
            this.RoundAddControlPanel3.Controls.Add(this.RemoveRoundButton, 1, 0);
            this.RoundAddControlPanel3.Controls.Add(this.AddRoundButton, 0, 0);
            this.RoundAddControlPanel3.Name = "RoundAddControlPanel3";
            // 
            // RemoveRoundButton
            // 
            resources.ApplyResources(this.RemoveRoundButton, "RemoveRoundButton");
            this.RemoveRoundButton.Name = "RemoveRoundButton";
            this.RemoveRoundButton.UseVisualStyleBackColor = true;
            this.RemoveRoundButton.Click += new System.EventHandler(this.RemoveRoundButton_Click);
            // 
            // AddRoundButton
            // 
            resources.ApplyResources(this.AddRoundButton, "AddRoundButton");
            this.AddRoundButton.Name = "AddRoundButton";
            this.AddRoundButton.UseVisualStyleBackColor = true;
            this.AddRoundButton.Click += new System.EventHandler(this.Map1AddRoundButton_Click);
            // 
            // RoundAddControlPanel2
            // 
            this.RoundAddControlPanel2.Controls.Add(this.RoundExtraInfoCheckBoxList);
            this.RoundAddControlPanel2.Controls.Add(this.RoundExtraInfoLabel);
            resources.ApplyResources(this.RoundAddControlPanel2, "RoundAddControlPanel2");
            this.RoundAddControlPanel2.Name = "RoundAddControlPanel2";
            // 
            // RoundExtraInfoCheckBoxList
            // 
            resources.ApplyResources(this.RoundExtraInfoCheckBoxList, "RoundExtraInfoCheckBoxList");
            this.RoundExtraInfoCheckBoxList.CheckOnClick = true;
            this.RoundExtraInfoCheckBoxList.FormattingEnabled = true;
            this.RoundExtraInfoCheckBoxList.Items.AddRange(new object[] {
            resources.GetString("RoundExtraInfoCheckBoxList.Items"),
            resources.GetString("RoundExtraInfoCheckBoxList.Items1"),
            resources.GetString("RoundExtraInfoCheckBoxList.Items2")});
            this.RoundExtraInfoCheckBoxList.Name = "RoundExtraInfoCheckBoxList";
            // 
            // RoundExtraInfoLabel
            // 
            resources.ApplyResources(this.RoundExtraInfoLabel, "RoundExtraInfoLabel");
            this.RoundExtraInfoLabel.Name = "RoundExtraInfoLabel";
            // 
            // RoundListView
            // 
            this.RoundListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Round,
            this.BombSpot,
            this.Plant,
            this.CounterDefuse,
            this.Extra,
            this.Win,
            this.Score});
            resources.ApplyResources(this.RoundListView, "RoundListView");
            this.RoundListView.FullRowSelect = true;
            this.RoundListView.GridLines = true;
            this.RoundListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.RoundListView.HideSelection = false;
            this.RoundListView.MultiSelect = false;
            this.RoundListView.Name = "RoundListView";
            this.RoundListView.ShowGroups = false;
            this.RoundListView.UseCompatibleStateImageBehavior = false;
            this.RoundListView.View = System.Windows.Forms.View.Details;
            this.RoundListView.ColumnWidthChanging += new System.Windows.Forms.ColumnWidthChangingEventHandler(this.RoundListView_ColumnWidthChanging);
            // 
            // Round
            // 
            resources.ApplyResources(this.Round, "Round");
            // 
            // BombSpot
            // 
            resources.ApplyResources(this.BombSpot, "BombSpot");
            // 
            // Plant
            // 
            resources.ApplyResources(this.Plant, "Plant");
            // 
            // CounterDefuse
            // 
            resources.ApplyResources(this.CounterDefuse, "CounterDefuse");
            // 
            // Extra
            // 
            resources.ApplyResources(this.Extra, "Extra");
            // 
            // Win
            // 
            resources.ApplyResources(this.Win, "Win");
            // 
            // Score
            // 
            resources.ApplyResources(this.Score, "Score");
            // 
            // CopyrightLabel
            // 
            resources.ApplyResources(this.CopyrightLabel, "CopyrightLabel");
            this.CopyrightLabel.Name = "CopyrightLabel";
            // 
            // MainTabs
            // 
            this.MainLayoutPanel.SetColumnSpan(this.MainTabs, 2);
            this.MainTabs.Controls.Add(this.MatchDataTab);
            this.MainTabs.Controls.Add(this.OperatorBanTab);
            this.MainTabs.Controls.Add(this.MapDataTab);
            resources.ApplyResources(this.MainTabs, "MainTabs");
            this.MainTabs.HotTrack = true;
            this.MainTabs.Name = "MainTabs";
            this.MainTabs.SelectedIndex = 0;
            // 
            // MatchDataTab
            // 
            resources.ApplyResources(this.MatchDataTab, "MatchDataTab");
            this.MatchDataTab.Controls.Add(this.MatchDataPanel);
            this.MatchDataTab.Name = "MatchDataTab";
            this.MatchDataTab.UseVisualStyleBackColor = true;
            // 
            // MatchDataPanel
            // 
            resources.ApplyResources(this.MatchDataPanel, "MatchDataPanel");
            this.MatchDataPanel.Controls.Add(this.MatchInfoGroupBox, 0, 0);
            this.MatchDataPanel.Controls.Add(this.TeamInfoGroupBox, 0, 1);
            this.MatchDataPanel.Controls.Add(this.MapInfoGroupBox, 1, 0);
            this.MatchDataPanel.Controls.Add(this.ExtendedMatchInfoGroup, 2, 0);
            this.MatchDataPanel.Controls.Add(this.MatchConfirmButton, 1, 1);
            this.MatchDataPanel.Controls.Add(this.CastersNotesGroup, 2, 1);
            this.MatchDataPanel.Name = "MatchDataPanel";
            // 
            // MapInfoGroupBox
            // 
            resources.ApplyResources(this.MapInfoGroupBox, "MapInfoGroupBox");
            this.MapInfoGroupBox.Controls.Add(this.Map3ComboBox);
            this.MapInfoGroupBox.Controls.Add(this.Map2ComboBox);
            this.MapInfoGroupBox.Controls.Add(this.Map3Label);
            this.MapInfoGroupBox.Controls.Add(this.Map2Label);
            this.MapInfoGroupBox.Controls.Add(this.Map1ComboBox);
            this.MapInfoGroupBox.Controls.Add(this.Map1Label);
            this.MapInfoGroupBox.Controls.Add(this.SpielModusComboBox);
            this.MapInfoGroupBox.Controls.Add(this.GameModeLabel);
            this.MapInfoGroupBox.Name = "MapInfoGroupBox";
            this.MapInfoGroupBox.TabStop = false;
            // 
            // Map3ComboBox
            // 
            resources.ApplyResources(this.Map3ComboBox, "Map3ComboBox");
            this.Map3ComboBox.FormattingEnabled = true;
            this.Map3ComboBox.Name = "Map3ComboBox";
            // 
            // Map2ComboBox
            // 
            resources.ApplyResources(this.Map2ComboBox, "Map2ComboBox");
            this.Map2ComboBox.FormattingEnabled = true;
            this.Map2ComboBox.Name = "Map2ComboBox";
            this.Map2ComboBox.SelectedIndexChanged += new System.EventHandler(this.Map2ComboBox_SelectedIndexChanged);
            // 
            // Map3Label
            // 
            resources.ApplyResources(this.Map3Label, "Map3Label");
            this.Map3Label.Name = "Map3Label";
            // 
            // Map2Label
            // 
            resources.ApplyResources(this.Map2Label, "Map2Label");
            this.Map2Label.Name = "Map2Label";
            // 
            // Map1ComboBox
            // 
            resources.ApplyResources(this.Map1ComboBox, "Map1ComboBox");
            this.Map1ComboBox.FormattingEnabled = true;
            this.Map1ComboBox.Name = "Map1ComboBox";
            this.Map1ComboBox.SelectedIndexChanged += new System.EventHandler(this.Map1ComboBox_SelectedIndexChanged);
            // 
            // Map1Label
            // 
            resources.ApplyResources(this.Map1Label, "Map1Label");
            this.Map1Label.Name = "Map1Label";
            // 
            // SpielModusComboBox
            // 
            resources.ApplyResources(this.SpielModusComboBox, "SpielModusComboBox");
            this.SpielModusComboBox.FormattingEnabled = true;
            this.SpielModusComboBox.Items.AddRange(new object[] {
            resources.GetString("SpielModusComboBox.Items"),
            resources.GetString("SpielModusComboBox.Items1"),
            resources.GetString("SpielModusComboBox.Items2")});
            this.SpielModusComboBox.Name = "SpielModusComboBox";
            this.SpielModusComboBox.SelectedIndexChanged += new System.EventHandler(this.SpielModusComboBox_SelectedIndexChanged);
            // 
            // GameModeLabel
            // 
            resources.ApplyResources(this.GameModeLabel, "GameModeLabel");
            this.GameModeLabel.Name = "GameModeLabel";
            // 
            // ExtendedMatchInfoGroup
            // 
            this.ExtendedMatchInfoGroup.Controls.Add(this.ExtendedMatchInfoTextBox);
            resources.ApplyResources(this.ExtendedMatchInfoGroup, "ExtendedMatchInfoGroup");
            this.ExtendedMatchInfoGroup.Name = "ExtendedMatchInfoGroup";
            this.ExtendedMatchInfoGroup.TabStop = false;
            // 
            // ExtendedMatchInfoTextBox
            // 
            resources.ApplyResources(this.ExtendedMatchInfoTextBox, "ExtendedMatchInfoTextBox");
            this.ExtendedMatchInfoTextBox.Name = "ExtendedMatchInfoTextBox";
            this.ExtendedMatchInfoTextBox.TextChanged += new System.EventHandler(this.ExtendedMatchInfoTextBox_TextChanged);
            // 
            // MatchConfirmButton
            // 
            resources.ApplyResources(this.MatchConfirmButton, "MatchConfirmButton");
            this.MatchConfirmButton.Name = "MatchConfirmButton";
            this.MatchConfirmButton.UseVisualStyleBackColor = true;
            this.MatchConfirmButton.Click += new System.EventHandler(this.MatchConfirmButton_Click);
            // 
            // CastersNotesGroup
            // 
            this.CastersNotesGroup.Controls.Add(this.CastersNotesMatchDataTextBox);
            resources.ApplyResources(this.CastersNotesGroup, "CastersNotesGroup");
            this.CastersNotesGroup.Name = "CastersNotesGroup";
            this.CastersNotesGroup.TabStop = false;
            // 
            // CastersNotesMatchDataTextBox
            // 
            resources.ApplyResources(this.CastersNotesMatchDataTextBox, "CastersNotesMatchDataTextBox");
            this.CastersNotesMatchDataTextBox.Name = "CastersNotesMatchDataTextBox";
            this.CastersNotesMatchDataTextBox.TextChanged += new System.EventHandler(this.CastersNotesMatchDataTextBox_TextChanged);
            // 
            // OperatorBanTab
            // 
            this.OperatorBanTab.Controls.Add(this.OperatorBanPanel);
            resources.ApplyResources(this.OperatorBanTab, "OperatorBanTab");
            this.OperatorBanTab.Name = "OperatorBanTab";
            this.OperatorBanTab.UseVisualStyleBackColor = true;
            // 
            // OperatorBanPanel
            // 
            resources.ApplyResources(this.OperatorBanPanel, "OperatorBanPanel");
            this.OperatorBanPanel.Controls.Add(this.groupBox1, 0, 1);
            this.OperatorBanPanel.Controls.Add(this.BlueBanGroup, 0, 0);
            this.OperatorBanPanel.Controls.Add(this.OrangeBanGroup, 1, 0);
            this.OperatorBanPanel.Controls.Add(this.BanConfirmButton, 0, 1);
            this.OperatorBanPanel.Name = "OperatorBanPanel";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CastersNotesOperatorBansTextBox);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // CastersNotesOperatorBansTextBox
            // 
            resources.ApplyResources(this.CastersNotesOperatorBansTextBox, "CastersNotesOperatorBansTextBox");
            this.CastersNotesOperatorBansTextBox.Name = "CastersNotesOperatorBansTextBox";
            this.CastersNotesOperatorBansTextBox.TextChanged += new System.EventHandler(this.CastersNotesOperatorBansTextBox_TextChanged);
            // 
            // BlueBanGroup
            // 
            this.BlueBanGroup.Controls.Add(this.DEFBanLabel1);
            this.BlueBanGroup.Controls.Add(this.ATKBanLabel1);
            this.BlueBanGroup.Controls.Add(this.TeamBlauATKBanComboBox);
            this.BlueBanGroup.Controls.Add(this.TeamBlauDEFBanComboBox);
            resources.ApplyResources(this.BlueBanGroup, "BlueBanGroup");
            this.BlueBanGroup.Name = "BlueBanGroup";
            this.BlueBanGroup.TabStop = false;
            // 
            // DEFBanLabel1
            // 
            resources.ApplyResources(this.DEFBanLabel1, "DEFBanLabel1");
            this.DEFBanLabel1.Name = "DEFBanLabel1";
            // 
            // ATKBanLabel1
            // 
            resources.ApplyResources(this.ATKBanLabel1, "ATKBanLabel1");
            this.ATKBanLabel1.Name = "ATKBanLabel1";
            // 
            // TeamBlauATKBanComboBox
            // 
            resources.ApplyResources(this.TeamBlauATKBanComboBox, "TeamBlauATKBanComboBox");
            this.TeamBlauATKBanComboBox.FormattingEnabled = true;
            this.TeamBlauATKBanComboBox.Name = "TeamBlauATKBanComboBox";
            // 
            // TeamBlauDEFBanComboBox
            // 
            resources.ApplyResources(this.TeamBlauDEFBanComboBox, "TeamBlauDEFBanComboBox");
            this.TeamBlauDEFBanComboBox.FormattingEnabled = true;
            this.TeamBlauDEFBanComboBox.Name = "TeamBlauDEFBanComboBox";
            // 
            // OrangeBanGroup
            // 
            this.OrangeBanGroup.Controls.Add(this.DEFBanLabel2);
            this.OrangeBanGroup.Controls.Add(this.ATKBanLabel2);
            this.OrangeBanGroup.Controls.Add(this.TeamOrangeDEFBanComboBox);
            this.OrangeBanGroup.Controls.Add(this.TeamOrangeATKBanComboBox);
            resources.ApplyResources(this.OrangeBanGroup, "OrangeBanGroup");
            this.OrangeBanGroup.Name = "OrangeBanGroup";
            this.OrangeBanGroup.TabStop = false;
            // 
            // DEFBanLabel2
            // 
            resources.ApplyResources(this.DEFBanLabel2, "DEFBanLabel2");
            this.DEFBanLabel2.Name = "DEFBanLabel2";
            // 
            // ATKBanLabel2
            // 
            resources.ApplyResources(this.ATKBanLabel2, "ATKBanLabel2");
            this.ATKBanLabel2.Name = "ATKBanLabel2";
            // 
            // TeamOrangeDEFBanComboBox
            // 
            resources.ApplyResources(this.TeamOrangeDEFBanComboBox, "TeamOrangeDEFBanComboBox");
            this.TeamOrangeDEFBanComboBox.FormattingEnabled = true;
            this.TeamOrangeDEFBanComboBox.Name = "TeamOrangeDEFBanComboBox";
            // 
            // TeamOrangeATKBanComboBox
            // 
            resources.ApplyResources(this.TeamOrangeATKBanComboBox, "TeamOrangeATKBanComboBox");
            this.TeamOrangeATKBanComboBox.FormattingEnabled = true;
            this.TeamOrangeATKBanComboBox.Name = "TeamOrangeATKBanComboBox";
            // 
            // BanConfirmButton
            // 
            resources.ApplyResources(this.BanConfirmButton, "BanConfirmButton");
            this.BanConfirmButton.Name = "BanConfirmButton";
            this.BanConfirmButton.UseVisualStyleBackColor = true;
            this.BanConfirmButton.Click += new System.EventHandler(this.BanConfirmButton_Click);
            // 
            // MapDataTab
            // 
            this.MapDataTab.Controls.Add(this.RoundPanel);
            resources.ApplyResources(this.MapDataTab, "MapDataTab");
            this.MapDataTab.Name = "MapDataTab";
            this.MapDataTab.UseVisualStyleBackColor = true;
            // 
            // RoundPanel
            // 
            resources.ApplyResources(this.RoundPanel, "RoundPanel");
            this.RoundPanel.Controls.Add(this.CastersNotesRoundsGroup, 0, 1);
            this.RoundPanel.Controls.Add(this.EndMapButton, 1, 0);
            this.RoundPanel.Controls.Add(this.RoundListView, 0, 0);
            this.RoundPanel.Controls.Add(this.AddRoundGroupBox, 0, 1);
            this.RoundPanel.Name = "RoundPanel";
            // 
            // CastersNotesRoundsGroup
            // 
            this.CastersNotesRoundsGroup.Controls.Add(this.CastersNotesRoundsTextBox);
            resources.ApplyResources(this.CastersNotesRoundsGroup, "CastersNotesRoundsGroup");
            this.CastersNotesRoundsGroup.Name = "CastersNotesRoundsGroup";
            this.CastersNotesRoundsGroup.TabStop = false;
            // 
            // CastersNotesRoundsTextBox
            // 
            resources.ApplyResources(this.CastersNotesRoundsTextBox, "CastersNotesRoundsTextBox");
            this.CastersNotesRoundsTextBox.Name = "CastersNotesRoundsTextBox";
            this.CastersNotesRoundsTextBox.TextChanged += new System.EventHandler(this.CastersNotesRoundsTextBox_TextChanged);
            // 
            // EndMapButton
            // 
            resources.ApplyResources(this.EndMapButton, "EndMapButton");
            this.EndMapButton.Name = "EndMapButton";
            this.EndMapButton.UseVisualStyleBackColor = true;
            this.EndMapButton.Click += new System.EventHandler(this.EndMapButton_Click);
            // 
            // MainLayoutPanel
            // 
            resources.ApplyResources(this.MainLayoutPanel, "MainLayoutPanel");
            this.MainLayoutPanel.Controls.Add(this.MainTabs, 0, 0);
            this.MainLayoutPanel.Controls.Add(this.QuitButton, 1, 2);
            this.MainLayoutPanel.Controls.Add(this.CopyrightLabel, 0, 2);
            this.MainLayoutPanel.Controls.Add(this.StatusLabel, 0, 1);
            this.MainLayoutPanel.Name = "MainLayoutPanel";
            // 
            // StatusLabel
            // 
            resources.ApplyResources(this.StatusLabel, "StatusLabel");
            this.MainLayoutPanel.SetColumnSpan(this.StatusLabel, 2);
            this.StatusLabel.Name = "StatusLabel";
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.MainLayoutPanel);
            this.Controls.Add(this.MenuStrip);
            this.KeyPreview = true;
            this.MainMenuStrip = this.MenuStrip;
            this.Name = "MainForm";
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.TeamInfoGroupBox.ResumeLayout(false);
            this.TeamInfoGroupBox.PerformLayout();
            this.MenuStrip.ResumeLayout(false);
            this.MenuStrip.PerformLayout();
            this.MatchInfoGroupBox.ResumeLayout(false);
            this.MatchInfoGroupBox.PerformLayout();
            this.AddRoundGroupBox.ResumeLayout(false);
            this.RoundAddPanel.ResumeLayout(false);
            this.RoundAddControlPanel1.ResumeLayout(false);
            this.RoundAddControlPanel1.PerformLayout();
            this.PlantRadioButtonPanel.ResumeLayout(false);
            this.PlantRadioButtonPanel.PerformLayout();
            this.RoundAddControlPanel3.ResumeLayout(false);
            this.RoundAddControlPanel2.ResumeLayout(false);
            this.RoundAddControlPanel2.PerformLayout();
            this.MainTabs.ResumeLayout(false);
            this.MatchDataTab.ResumeLayout(false);
            this.MatchDataTab.PerformLayout();
            this.MatchDataPanel.ResumeLayout(false);
            this.MapInfoGroupBox.ResumeLayout(false);
            this.MapInfoGroupBox.PerformLayout();
            this.ExtendedMatchInfoGroup.ResumeLayout(false);
            this.ExtendedMatchInfoGroup.PerformLayout();
            this.CastersNotesGroup.ResumeLayout(false);
            this.CastersNotesGroup.PerformLayout();
            this.OperatorBanTab.ResumeLayout(false);
            this.OperatorBanPanel.ResumeLayout(false);
            this.OperatorBanPanel.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.BlueBanGroup.ResumeLayout(false);
            this.BlueBanGroup.PerformLayout();
            this.OrangeBanGroup.ResumeLayout(false);
            this.OrangeBanGroup.PerformLayout();
            this.MapDataTab.ResumeLayout(false);
            this.RoundPanel.ResumeLayout(false);
            this.CastersNotesRoundsGroup.ResumeLayout(false);
            this.CastersNotesRoundsGroup.PerformLayout();
            this.MainLayoutPanel.ResumeLayout(false);
            this.MainLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button QuitButton;
        private System.Windows.Forms.TextBox TeamOrangeTextBox;
        private System.Windows.Forms.TextBox TeamBlueTextBox;
        private System.Windows.Forms.Label TeamBlueLabel;
        private System.Windows.Forms.Label TeamOrangeLabel;
        private System.Windows.Forms.GroupBox TeamInfoGroupBox;
        private System.Windows.Forms.MenuStrip MenuStrip;
        private System.Windows.Forms.ToolStripMenuItem FileMenu;
        private System.Windows.Forms.ToolStripMenuItem NewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OpenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SaveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SaveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem QuitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem EditMenu;
        private System.Windows.Forms.ToolStripMenuItem ResetMapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ResetMatchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HelpMenu;
        private System.Windows.Forms.ToolStripMenuItem AboutToolStripMenuItem;
        private System.Windows.Forms.GroupBox MatchInfoGroupBox;
        private System.Windows.Forms.TextBox MatchTitleTextBox;
        private System.Windows.Forms.Label MatchTitleLabel;
        private System.Windows.Forms.GroupBox AddRoundGroupBox;
        private System.Windows.Forms.CheckedListBox RoundExtraInfoCheckBoxList;
        private System.Windows.Forms.Panel PlantRadioButtonPanel;
        private System.Windows.Forms.RadioButton RoundPlantYesRadioButton;
        private System.Windows.Forms.RadioButton RoundPlantNoRadioButton;
        private System.Windows.Forms.RadioButton RoundPlantDeniedNoRadioButton;
        private System.Windows.Forms.RadioButton RoundPlantDeniedYesRadioButton;
        private System.Windows.Forms.ComboBox RoundWinComboBox;
        private System.Windows.Forms.ComboBox RoundBombSpotComboBox;
        private System.Windows.Forms.Label RoundWinLabel;
        private System.Windows.Forms.Label RoundExtraInfoLabel;
        private System.Windows.Forms.Label RoundPlantDeniedLabel;
        private System.Windows.Forms.Label RoundPlantLabel;
        private System.Windows.Forms.Label RoundBombSpotLabel;
        private System.Windows.Forms.Button AddRoundButton;
        private System.Windows.Forms.ListView RoundListView;
        private System.Windows.Forms.ColumnHeader Round;
        private System.Windows.Forms.ColumnHeader BombSpot;
        private System.Windows.Forms.ColumnHeader Plant;
        private System.Windows.Forms.ColumnHeader CounterDefuse;
        private System.Windows.Forms.ColumnHeader Extra;
        private System.Windows.Forms.ColumnHeader Win;
        private System.Windows.Forms.ColumnHeader Score;
        private System.Windows.Forms.Label CopyrightLabel;
        private System.Windows.Forms.Button RemoveRoundButton;
        private System.Windows.Forms.ToolStripMenuItem UpdateCheckToolStripMenuItem;
        private System.Windows.Forms.TabControl MainTabs;
        private System.Windows.Forms.TabPage MatchDataTab;
        private System.Windows.Forms.TabPage MapDataTab;
        private System.Windows.Forms.TextBox MatchURLTextBox;
        private System.Windows.Forms.Label MatchURLLabel;
        private System.Windows.Forms.Button EndMapButton;
        private System.Windows.Forms.TabPage OperatorBanTab;
        private System.Windows.Forms.GroupBox BlueBanGroup;
        private System.Windows.Forms.Button BanConfirmButton;
        private System.Windows.Forms.ComboBox TeamOrangeDEFBanComboBox;
        private System.Windows.Forms.ComboBox TeamOrangeATKBanComboBox;
        private System.Windows.Forms.ComboBox TeamBlauDEFBanComboBox;
        private System.Windows.Forms.ComboBox TeamBlauATKBanComboBox;
        private System.Windows.Forms.Label DEFBanLabel1;
        private System.Windows.Forms.Label ATKBanLabel1;
        private System.Windows.Forms.TableLayoutPanel MatchDataPanel;
        private System.Windows.Forms.TableLayoutPanel MainLayoutPanel;
        private System.Windows.Forms.GroupBox ExtendedMatchInfoGroup;
        private System.Windows.Forms.TextBox ExtendedMatchInfoTextBox;
        private System.Windows.Forms.GroupBox MapInfoGroupBox;
        private System.Windows.Forms.ComboBox Map3ComboBox;
        private System.Windows.Forms.ComboBox Map2ComboBox;
        private System.Windows.Forms.Label Map3Label;
        private System.Windows.Forms.Label Map2Label;
        private System.Windows.Forms.ComboBox Map1ComboBox;
        private System.Windows.Forms.Label Map1Label;
        private System.Windows.Forms.ComboBox SpielModusComboBox;
        private System.Windows.Forms.Label GameModeLabel;
        private System.Windows.Forms.TableLayoutPanel OperatorBanPanel;
        private System.Windows.Forms.GroupBox OrangeBanGroup;
        private System.Windows.Forms.Label DEFBanLabel2;
        private System.Windows.Forms.Label ATKBanLabel2;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.TableLayoutPanel RoundPanel;
        private System.Windows.Forms.TableLayoutPanel RoundAddPanel;
        private System.Windows.Forms.Panel RoundAddControlPanel1;
        private System.Windows.Forms.TableLayoutPanel RoundAddControlPanel3;
        private System.Windows.Forms.Panel RoundAddControlPanel2;
        private System.Windows.Forms.ToolStripMenuItem ExportMatchToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ExportMarkdownToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExportCSVToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem languageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem englishDefaultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem germanDeutschToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox CastersNotesOperatorBansTextBox;
        private System.Windows.Forms.GroupBox CastersNotesRoundsGroup;
        private System.Windows.Forms.TextBox CastersNotesRoundsTextBox;
        private System.Windows.Forms.Button MatchConfirmButton;
        private System.Windows.Forms.GroupBox CastersNotesGroup;
        private System.Windows.Forms.TextBox CastersNotesMatchDataTextBox;
    }
}

