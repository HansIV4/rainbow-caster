﻿namespace RainbowCaster
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.SettingsMainLayout = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.OBSDynTxtBrowseButton = new System.Windows.Forms.Button();
            this.OBSDynTxtDirTextBox = new System.Windows.Forms.TextBox();
            this.LanguageLabel = new System.Windows.Forms.Label();
            this.LanguageComboBox = new System.Windows.Forms.ComboBox();
            this.OBSDynTxtLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.OBSDynTxtNoRB = new System.Windows.Forms.RadioButton();
            this.OBSDynTxtYesRB = new System.Windows.Forms.RadioButton();
            this.OBSDynTxtDirLabel = new System.Windows.Forms.Label();
            this.SaveButton = new System.Windows.Forms.Button();
            this.SavedLabel = new System.Windows.Forms.Label();
            this.SettingsMainLayout.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SettingsMainLayout
            // 
            resources.ApplyResources(this.SettingsMainLayout, "SettingsMainLayout");
            this.SettingsMainLayout.Controls.Add(this.tableLayoutPanel2, 1, 2);
            this.SettingsMainLayout.Controls.Add(this.LanguageLabel, 0, 0);
            this.SettingsMainLayout.Controls.Add(this.LanguageComboBox, 1, 0);
            this.SettingsMainLayout.Controls.Add(this.OBSDynTxtLabel, 0, 1);
            this.SettingsMainLayout.Controls.Add(this.panel1, 1, 1);
            this.SettingsMainLayout.Controls.Add(this.OBSDynTxtDirLabel, 0, 2);
            this.SettingsMainLayout.Controls.Add(this.SaveButton, 0, 3);
            this.SettingsMainLayout.Controls.Add(this.SavedLabel, 1, 3);
            this.SettingsMainLayout.Name = "SettingsMainLayout";
            // 
            // tableLayoutPanel2
            // 
            resources.ApplyResources(this.tableLayoutPanel2, "tableLayoutPanel2");
            this.tableLayoutPanel2.Controls.Add(this.OBSDynTxtBrowseButton, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.OBSDynTxtDirTextBox, 0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            // 
            // OBSDynTxtBrowseButton
            // 
            resources.ApplyResources(this.OBSDynTxtBrowseButton, "OBSDynTxtBrowseButton");
            this.OBSDynTxtBrowseButton.Name = "OBSDynTxtBrowseButton";
            this.OBSDynTxtBrowseButton.UseVisualStyleBackColor = true;
            this.OBSDynTxtBrowseButton.Click += new System.EventHandler(this.OBSDynTxtBrowseButton_Click);
            // 
            // OBSDynTxtDirTextBox
            // 
            resources.ApplyResources(this.OBSDynTxtDirTextBox, "OBSDynTxtDirTextBox");
            this.OBSDynTxtDirTextBox.Name = "OBSDynTxtDirTextBox";
            // 
            // LanguageLabel
            // 
            resources.ApplyResources(this.LanguageLabel, "LanguageLabel");
            this.LanguageLabel.Name = "LanguageLabel";
            // 
            // LanguageComboBox
            // 
            resources.ApplyResources(this.LanguageComboBox, "LanguageComboBox");
            this.LanguageComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.LanguageComboBox.FormattingEnabled = true;
            this.LanguageComboBox.Items.AddRange(new object[] {
            resources.GetString("LanguageComboBox.Items"),
            resources.GetString("LanguageComboBox.Items1")});
            this.LanguageComboBox.Name = "LanguageComboBox";
            // 
            // OBSDynTxtLabel
            // 
            resources.ApplyResources(this.OBSDynTxtLabel, "OBSDynTxtLabel");
            this.OBSDynTxtLabel.Name = "OBSDynTxtLabel";
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Controls.Add(this.OBSDynTxtNoRB);
            this.panel1.Controls.Add(this.OBSDynTxtYesRB);
            this.panel1.Name = "panel1";
            // 
            // OBSDynTxtNoRB
            // 
            resources.ApplyResources(this.OBSDynTxtNoRB, "OBSDynTxtNoRB");
            this.OBSDynTxtNoRB.Name = "OBSDynTxtNoRB";
            this.OBSDynTxtNoRB.TabStop = true;
            this.OBSDynTxtNoRB.UseVisualStyleBackColor = true;
            this.OBSDynTxtNoRB.CheckedChanged += new System.EventHandler(this.OBSDynTxtNoRB_CheckedChanged);
            // 
            // OBSDynTxtYesRB
            // 
            resources.ApplyResources(this.OBSDynTxtYesRB, "OBSDynTxtYesRB");
            this.OBSDynTxtYesRB.Name = "OBSDynTxtYesRB";
            this.OBSDynTxtYesRB.TabStop = true;
            this.OBSDynTxtYesRB.UseVisualStyleBackColor = true;
            this.OBSDynTxtYesRB.CheckedChanged += new System.EventHandler(this.OBSDynTxtYesRB_CheckedChanged);
            // 
            // OBSDynTxtDirLabel
            // 
            resources.ApplyResources(this.OBSDynTxtDirLabel, "OBSDynTxtDirLabel");
            this.OBSDynTxtDirLabel.Name = "OBSDynTxtDirLabel";
            // 
            // SaveButton
            // 
            resources.ApplyResources(this.SaveButton, "SaveButton");
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // SavedLabel
            // 
            resources.ApplyResources(this.SavedLabel, "SavedLabel");
            this.SavedLabel.Name = "SavedLabel";
            // 
            // SettingsForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.SettingsMainLayout);
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.SettingsMainLayout.ResumeLayout(false);
            this.SettingsMainLayout.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel SettingsMainLayout;
        private System.Windows.Forms.Label LanguageLabel;
        private System.Windows.Forms.ComboBox LanguageComboBox;
        private System.Windows.Forms.Label OBSDynTxtLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton OBSDynTxtNoRB;
        private System.Windows.Forms.RadioButton OBSDynTxtYesRB;
        private System.Windows.Forms.Label OBSDynTxtDirLabel;
        private System.Windows.Forms.Button OBSDynTxtBrowseButton;
        private System.Windows.Forms.TextBox OBSDynTxtDirTextBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Label SavedLabel;
    }
}