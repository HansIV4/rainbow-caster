﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RainbowCaster.Properties;
using RainbowCaster.Properties.strings;

namespace RainbowCaster {
    public partial class SettingsForm : Form {
        public SettingsForm() {
            InitializeComponent();
            InitSettings();
        }

        private void InitSettings() {
            OBSDynTxtDirTextBox.Text = Settings.Default.OBSDynamicTextBaseDir;
            LanguageComboBox.SelectedItem = Settings.Default.LanguageFull;
            
            if (Settings.Default.UseOBSDynamicText) {
                OBSDynTxtYesRB.Checked = true;
                OBSDynTxtBrowseButton.Enabled = true;
            }
            else {
                OBSDynTxtNoRB.Checked = true;
                OBSDynTxtBrowseButton.Enabled = false;
            }
        }

        private void OBSDynTxtYesRB_CheckedChanged(object sender, EventArgs e) {
            OBSDynTxtBrowseButton.Enabled = true;
        }

        private void OBSDynTxtNoRB_CheckedChanged(object sender, EventArgs e) {
            OBSDynTxtBrowseButton.Enabled = false;
        }

        private void OBSDynTxtBrowseButton_Click(object sender, EventArgs e) {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.ShowDialog();
            OBSDynTxtDirTextBox.Text = dialog.SelectedPath;
        }

        private void SaveButton_Click(object sender, EventArgs e) {
            Settings.Default.OBSDynamicTextBaseDir = OBSDynTxtDirTextBox.Text;
            Settings.Default.UseOBSDynamicText = OBSDynTxtYesRB.Checked;

            if (!LanguageComboBox.Text.Equals(Settings.Default.LanguageFull)) {

                string lang = "";

                if (LanguageComboBox.Text.Contains("en-US")) {
                    lang = "en-US";
                }    
                else if (LanguageComboBox.Text.Contains("de-DE")) {
                    lang = "de-DE";
                }
                
                Settings.Default.Language = lang;
                Settings.Default.LanguageFull = LanguageComboBox.Text;
                Settings.Default.Save();
                Application.Restart();
            }
            
            Settings.Default.Save();
            SavedLabel.Visible = true;
        }

    }
}