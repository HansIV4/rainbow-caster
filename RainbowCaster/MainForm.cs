﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Threading;
using System.Resources;
using RainbowCaster.Maps;
using RainbowCaster.Operators;
using RainbowCaster.Properties;
using RainbowCaster.Properties.strings;

namespace RainbowCaster {
    public partial class MainForm : Form {
        private string Language = Settings.Default.Language;
        private OBSDynamicTextEditor OBSEd;

        private readonly Session GameSession;

        private string OldMapValue1, OldMapValue2;

        public MainForm() {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(Language);
            InitializeComponent();
            SetupControls();

            GameSession = new Session();
            if (Settings.Default.UseOBSDynamicText) {
                OBSEd = new OBSDynamicTextEditor(Settings.Default.OBSDynamicTextBaseDir);
                bool success = OBSEd.CreateEmtpyTextFiles();
                if (!success) {
                    MessageBox.Show(
                        Errors.OBSDynTextDirNotFound,
                        "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Settings.Default.UseOBSDynamicText = false;
                    Settings.Default.OBSDynamicTextBaseDir = String.Empty;
                    Settings.Default.Save();
                }
            }
        }

        #region Private Methods

        private void SetupControls() {
            //Setup Icon
            Icon = Resources.r6logoIcon;

            //Set DropDownLists
            SpielModusComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            Map1ComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            Map2ComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            Map3ComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            TeamBlauATKBanComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            TeamBlauDEFBanComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            TeamOrangeATKBanComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            TeamOrangeDEFBanComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            RoundBombSpotComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            RoundWinComboBox.DropDownStyle = ComboBoxStyle.DropDownList;


            //Setup Map Combo Boxes
            Map1ComboBox.Items.AddRange(MapPool.GetMapPool());
            Map2ComboBox.Items.AddRange(MapPool.GetMapPool());
            Map3ComboBox.Items.AddRange(MapPool.GetMapPool());
        }

        private void ClearMapElements() {
            //Reset Operator Ban Group
            BlueBanGroup.Enabled = true;
            TeamBlauATKBanComboBox.SelectedItem = null;
            TeamOrangeATKBanComboBox.SelectedItem = null;
            TeamBlauDEFBanComboBox.SelectedItem = null;
            TeamOrangeDEFBanComboBox.SelectedItem = null;

            //Reset ListView
            foreach (ListViewItem i in RoundListView.Items) {
                RoundListView.Items.Remove(i);
            }

            //Disable ListView & Round Tools
            RoundPanel.Enabled = false;
        }

        private void CheckforUpdates(bool manualCheck) {
           /* string currentVersion = Resources.CurrentVersion;

            WebClient wc = new WebClient();
            string remoteVersion = wc.DownloadString(@"https://gitlab.com/sthorsten/rainbow-caster/raw/master/VERSION");

            int compare = currentVersion.CompareTo(remoteVersion);
            bool needsUpdate = compare < 0;
            if (!needsUpdate) {
                if (manualCheck)
                    MessageBox.Show(main.NoNewVersion, main.NoNewVersionCaption, MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                return;
            }

            string NewUpdate = string.Format(main.UpdateInfo, Environment.NewLine, currentVersion, remoteVersion);
            DialogResult result = MessageBox.Show(NewUpdate, main.UpdateAvailable, MessageBoxButtons.YesNo,
                MessageBoxIcon.Information);
            if (result == DialogResult.No) return;

            System.Diagnostics.Process.Start(
                "https://gitlab.com/sthorsten/rainbow-caster/");
            Thread.Sleep(1000);
            Dispose(); */
        }

        private void UpdateStatus(string text, bool error = false) {
            if (error) {
                StatusLabel.BackColor = Color.Salmon;
                StatusLabel.Text = StatusMessages.Error + ": " + text;
            }
            else {
                StatusLabel.BackColor = Color.LightGreen;
                StatusLabel.Text = StatusMessages.Status + ": " + text;
            }

            Update();
            Thread.Sleep(200);
            StatusLabel.BackColor = Color.Transparent;
        }

        private void ExportData(string type) {
            string export = "";
            SaveFileDialog saveFileDialog = new SaveFileDialog {
                Title = main.SaveMatchData,
                RestoreDirectory = true
            };

            if (type.Equals("csv")) {
                export = GameSession.ExportAsCSV();
                saveFileDialog.FileName = "Match1.csv";
                saveFileDialog.Filter = "CSV File (*.csv)|*.csv";
            }

            if (type.Equals("md")) {
                export = GameSession.ExportAsMarkdown();
                saveFileDialog.FileName = "Match1.md";
                saveFileDialog.Filter = "Markdown File (*.md)|*.md";
            }

            DialogResult rs = saveFileDialog.ShowDialog();
            if (rs == DialogResult.OK) {
                StreamWriter writer = new StreamWriter(saveFileDialog.OpenFile());
                writer.Write(export);
                writer.Dispose();
                writer.Close();
                UpdateStatus(main.ExportSuccessful);
            }
        }

        private void ChangeLanguage(string lang, string langFull) {
            Language = lang;
            Settings.Default.LanguageFull = langFull;
            Settings.Default.Language = lang;
            Settings.Default.Save();
            Application.Restart();
        }

        #endregion

        #region Event Handling

        /*
         * Overriding Default OnFormClosing() to Show Confirmation Dialog
         */
        protected override void OnFormClosing(FormClosingEventArgs e) {
            if (e.CloseReason != CloseReason.UserClosing) return;

            //Override Default Close Button and show Confimation Dialog
            var confirm = MessageBox.Show(StatusMessages.ConfirmExit, StatusMessages.ConfirmExitCaption,
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);
            if (confirm == DialogResult.No) {
                e.Cancel = true;
                base.OnFormClosing(e);
            }
        }

        private void QuitButton_Click(object sender, EventArgs e) {
            var confirm = MessageBox.Show(StatusMessages.ConfirmExit, StatusMessages.ConfirmExitCaption,
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);
            if (confirm == DialogResult.Yes) Dispose();
        }

        private void RoundListView_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e) {
            //Prevent ListView from being resized
            e.Cancel = true;
            e.NewWidth = RoundListView.Columns[e.ColumnIndex].Width;
        }

        private void SpielModusComboBox_SelectedIndexChanged(object sender, EventArgs e) {
            if (!string.IsNullOrEmpty(SpielModusComboBox.Text)) {
                //At least one Map
                Map1ComboBox.Enabled = true;
                Map1Label.Enabled = true;

                //Exactly one Map
                if (SpielModusComboBox.Text == "Best Of 1") {
                    Map2ComboBox.Enabled = false;
                    Map3ComboBox.Enabled = false;
                    Map2Label.Enabled = false;
                    Map3Label.Enabled = false;
                    return;
                }
            }

            if (SpielModusComboBox.Text == "Best Of 2" || SpielModusComboBox.Text == "Best Of 3") {
                //At least 2 Maps
                Map2ComboBox.Enabled = true;
                Map2Label.Enabled = true;

                //Exactly 2 Maps
                if (SpielModusComboBox.Text == "Best Of 2") {
                    Map3ComboBox.Enabled = false;
                    Map3Label.Enabled = false;
                    //3 Maps    
                }
                else {
                    Map3ComboBox.Enabled = true;
                    Map3Label.Enabled = true;
                }
            }
        }

        private void MatchConfirmButton_Click(object sender, EventArgs e) {
            //Check Title
            if (string.IsNullOrEmpty(MatchTitleTextBox.Text)) {
                UpdateStatus(StatusMessages.TitleEmptyStatus, true);
                return;
            }

            //Check Teamnames
            if (string.IsNullOrEmpty(TeamOrangeTextBox.Text)) {
                UpdateStatus(string.Format(StatusMessages.TeamNameEmpty, StatusMessages.Orange), true);
                return;
            }

            if (string.IsNullOrEmpty(TeamBlueTextBox.Text)) {
                UpdateStatus(string.Format(StatusMessages.TeamNameEmpty, StatusMessages.Blue), true);
                return;
            }

            //Check Map Settings
            if (string.IsNullOrEmpty(Map1ComboBox.Text)) {
                UpdateStatus(StatusMessages.MapSelectionEmpty, true);
                return;
            }

            if (Map2ComboBox.Enabled) {
                if (string.IsNullOrEmpty(Map2ComboBox.Text)) {
                    UpdateStatus(string.Format(StatusMessages.MapSelectionEmptySpecific, "2"), true);
                    return;
                }
            }

            if (Map3ComboBox.Enabled) {
                if (string.IsNullOrEmpty(Map3ComboBox.Text)) {
                    UpdateStatus(string.Format(StatusMessages.MapSelectionEmptySpecific, "3"), true);
                    return;
                }
            }

            //Set Game Session Details
            GameSession.SetTeamNames(TeamBlueTextBox.Text, TeamOrangeTextBox.Text);
            GameSession.SetMatchInformation(MatchTitleTextBox.Text, MatchURLTextBox.Text,
                ExtendedMatchInfoTextBox.Text);
            GameSession.InitMaps(new[] {Map1ComboBox.Text, Map2ComboBox.Text, Map3ComboBox.Text});


            //Set UI Element Text
            BlueBanGroup.Text = GameSession.TeamBlue;
            OrangeBanGroup.Text = GameSession.TeamOrange;

            RoundWinComboBox.Items.Add(GameSession.TeamBlue);
            RoundWinComboBox.Items.Add(GameSession.TeamOrange);

            RoundBombSpotComboBox.Items.AddRange(GameSession.CurrentMap.GetBombSpots());

            TeamBlauATKBanComboBox.Items.AddRange(ATKOperators.GetATKOperators());
            TeamBlauDEFBanComboBox.Items.AddRange(DEFOperators.GetDEFOperators());

            TeamOrangeATKBanComboBox.Items.AddRange(ATKOperators.GetATKOperators());
            TeamOrangeDEFBanComboBox.Items.AddRange(DEFOperators.GetDEFOperators());

            BanConfirmButton.Text =
                string.Format(main.BanConfirmButtonText, "\"" + GameSession.CurrentMap.GetMapName() + "\"");

            //Update Dynamic Text Files if selected
            if (Settings.Default.UseOBSDynamicText) {
                OBSEd.SetContent(OBSDynamicTextEditor.FileName.ScoreBlue, "0");
                OBSEd.SetContent(OBSDynamicTextEditor.FileName.ScoreOrange, "0");
                OBSEd.SetContent(OBSDynamicTextEditor.FileName.NextMap,
                    string.Format(main.NextMapPrefix + GameSession.GetNextMap(), "2"));
            }


            //Show Confirmation
            UpdateStatus(StatusMessages.MatchDataConfirmed);

            //Enable Operator Bans Tab
            OperatorBanPanel.Enabled = true;
            MainTabs.SelectTab(1);
        }

        private void beendenToolStripMenuItem_Click(object sender, EventArgs e) {
            var confirm = MessageBox.Show(StatusMessages.ConfirmExit, StatusMessages.ConfirmExitCaption,
                MessageBoxButtons.YesNo);
            if (confirm == DialogResult.Yes) Dispose();
        }

        private void überDasProgrammToolStripMenuItem_Click(object sender, EventArgs e) {
            new AboutForm().ShowDialog();
        }

        private void RemoveRoundButton_Click(object sender, EventArgs e) {
            GameSession.CurrentMap.RemoveRound(RoundListView.Items.Count);
            RoundListView.Items.RemoveAt(RoundListView.Items.Count - 1);

            if (RoundListView.Items.Count == 0) RemoveRoundButton.Enabled = false;

            UpdateStatus(StatusMessages.RemovedRound);
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e) {
            if (e.Control && e.KeyCode == Keys.E) AlsMarkdownToolStripMenuItem_Click(null, null);
            if (e.Control && e.KeyCode == Keys.Q) QuitButton_Click(null, null);
        }

        private void MainForm_Shown(object sender, EventArgs e) {
            CheckforUpdates(false);
        }

        private void AufUpdatesÜberprüfenToolStripMenuItem_Click(object sender, EventArgs e) {
            CheckforUpdates(true);
        }

        private void AlsCSVToolStripMenuItem_Click(object sender, EventArgs e) {
            ExportData("csv");
        }

        private void AlsMarkdownToolStripMenuItem_Click(object sender, EventArgs e) {
            ExportData("md");
        }

        private void BanConfirmButton_Click(object sender, EventArgs e) {
            //Check if Bans are emtpy
            if (string.IsNullOrEmpty(TeamBlauATKBanComboBox.Text) ||
                string.IsNullOrEmpty(TeamOrangeATKBanComboBox.Text) ||
                string.IsNullOrEmpty(TeamBlauDEFBanComboBox.Text) ||
                string.IsNullOrEmpty(TeamOrangeDEFBanComboBox.Text)) {
                UpdateStatus(StatusMessages.EmptyOperatorSelection, true);
                return;
            }

            //Setup Bans
            GameSession.CurrentMap.SetBans(TeamBlauATKBanComboBox.Text, TeamOrangeATKBanComboBox.Text,
                TeamBlauDEFBanComboBox.Text, TeamOrangeDEFBanComboBox.Text);

            UpdateStatus(string.Format(StatusMessages.OperatorBansSaved,
                "\"" + GameSession.CurrentMap.GetMapName() + "\""));

            //Enable Round Tab
            RoundPanel.Enabled = true;
            MainTabs.SelectTab(2);
        }

        private void Map1AddRoundButton_Click(object sender, EventArgs e) {
            //Check Input
            if (string.IsNullOrEmpty(RoundBombSpotComboBox.Text)) {
                UpdateStatus(StatusMessages.SelectBombSpot, true);
                return;
            }

            if (string.IsNullOrEmpty(RoundWinComboBox.Text)) {
                UpdateStatus(StatusMessages.WinTeamEmtpy, true);
                return;
            }

            //Format win name
            string win = "";
            if (RoundWinComboBox.Text == GameSession.TeamBlue) win = StatusMessages.Blue;
            else if (RoundWinComboBox.Text == GameSession.TeamOrange) win = StatusMessages.Orange;

            //Format extra name
            string extra = "";
            foreach (String s in RoundExtraInfoCheckBoxList.CheckedItems) {
                extra += " " + s + " ";
            }

            if (extra == "") extra = "---";

            //Add round result to the Map Data
            List<string> RoundResult = GameSession.CurrentMap.AddRound(RoundBombSpotComboBox.Text,
                RoundPlantYesRadioButton.Checked, RoundPlantDeniedYesRadioButton.Checked, extra, win);

            //Add round result to the List View
            ListViewItem item = new ListViewItem(RoundResult[0]);
            RoundResult.Remove(RoundResult[0]);
            item.SubItems.AddRange(RoundResult.ToArray());
            RoundListView.Items.Add(item);

            //Reset Round Controls
            RoundBombSpotComboBox.SelectedItem = null;
            RoundPlantNoRadioButton.Checked = true;
            RoundPlantDeniedNoRadioButton.Checked = true;
            RoundExtraInfoCheckBoxList.SetItemChecked(0, false);
            RoundExtraInfoCheckBoxList.SetItemChecked(1, false);
            RoundExtraInfoCheckBoxList.SetItemChecked(2, false);
            RoundWinComboBox.SelectedItem = null;

            //Enable RemoveRound Button
            RemoveRoundButton.Enabled = true;

            UpdateStatus(string.Format(StatusMessages.RoundAdded, GameSession.CurrentMap.GetRoundCount()));
        }

        private void Map1ComboBox_SelectedIndexChanged(object sender, EventArgs e) {
            //Remove Selection from Maps 2 and 3
            if (!string.IsNullOrEmpty(Map1ComboBox.Text)) {
                if (!string.IsNullOrEmpty(OldMapValue1)) {
                    Map2ComboBox.Items.Add(OldMapValue1);
                    Map3ComboBox.Items.Add(OldMapValue1);
                }

                OldMapValue1 = Map1ComboBox.Text;
                Map2ComboBox.Items.Remove(Map1ComboBox.Text);
                Map3ComboBox.Items.Remove(Map1ComboBox.Text);
            }
        }

        private void Map2ComboBox_SelectedIndexChanged(object sender, EventArgs e) {
            //Remove Selection from Map 3
            if (!string.IsNullOrEmpty(Map2ComboBox.Text)) {
                if (!string.IsNullOrEmpty(OldMapValue2)) {
                    Map3ComboBox.Items.Add(OldMapValue2);
                }

                OldMapValue2 = Map2ComboBox.Text;
                Map3ComboBox.Items.Remove(Map2ComboBox.Text);
            }

            if (Settings.Default.UseOBSDynamicText) {
                OBSEd.SetContent(OBSDynamicTextEditor.FileName.NextMap,
                    string.Format(main.NextMapPrefix, "2") + Map2ComboBox.Text);
            }
        }


        private void EnglishDefaultToolStripMenuItem_Click(object sender, EventArgs e) {
            ChangeLanguage("en-US", englishDefaultToolStripMenuItem.Text);
        }

        private void GermanDeutschToolStripMenuItem_Click(object sender, EventArgs e) {
            ChangeLanguage("de-DE", germanDeutschToolStripMenuItem.Text);
        }

        private void SettingsToolStripMenuItem_Click(object sender, EventArgs e) {
            new SettingsForm().ShowDialog();

            // Init OBS Dynamic Text if Settings have been set to use it
            if (Settings.Default.UseOBSDynamicText) {
                OBSEd = new OBSDynamicTextEditor(Settings.Default.OBSDynamicTextBaseDir);
                bool success = OBSEd.CreateEmtpyTextFiles();
                if (!success) {
                    MessageBox.Show(
                        Errors.OBSDynTextDirNotFound,
                        "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Settings.Default.UseOBSDynamicText = false;
                    Settings.Default.OBSDynamicTextBaseDir = String.Empty;
                    Settings.Default.Save();
                }

                OBSEd.SetContent(OBSDynamicTextEditor.FileName.MatchTitle, MatchTitleTextBox.Text);
                OBSEd.SetContent(OBSDynamicTextEditor.FileName.MatchSubTitle, ExtendedMatchInfoTextBox.Text);
            }
        }

        private void MatchTitleTextBox_TextChanged(object sender, EventArgs e) {
            if (Settings.Default.UseOBSDynamicText) {
                OBSEd.SetContent(OBSDynamicTextEditor.FileName.MatchTitle, MatchTitleTextBox.Text);
            }
        }

        private void ExtendedMatchInfoTextBox_TextChanged(object sender, EventArgs e) {
            if (Settings.Default.UseOBSDynamicText) {
                OBSEd.SetContent(OBSDynamicTextEditor.FileName.MatchSubTitle, ExtendedMatchInfoTextBox.Text);
            }
        }

        private void ResetMapToolStripMenuItem_Click(object sender, EventArgs e) {
            GameSession.ResetCurrentMap();
            ClearMapElements();
        }

        private void CastersNotesMatchDataTextBox_TextChanged(object sender, EventArgs e)
        {
            GameSession.CastersNotes = CastersNotesMatchDataTextBox.Text;
            CastersNotesOperatorBansTextBox.Text = GameSession.CastersNotes;
            CastersNotesRoundsTextBox.Text = GameSession.CastersNotes;
        }

        private void CastersNotesOperatorBansTextBox_TextChanged(object sender, EventArgs e)
        {
            GameSession.CastersNotes = CastersNotesOperatorBansTextBox.Text;
            CastersNotesMatchDataTextBox.Text = GameSession.CastersNotes;
            CastersNotesRoundsTextBox.Text = GameSession.CastersNotes;
        }

        private void CastersNotesRoundsTextBox_TextChanged(object sender, EventArgs e)
        {
            GameSession.CastersNotes = CastersNotesRoundsTextBox.Text;
            CastersNotesMatchDataTextBox.Text = GameSession.CastersNotes;
            CastersNotesOperatorBansTextBox.Text = GameSession.CastersNotes;
        }

        private void EndMapButton_Click(object sender, EventArgs e) {
            //Update Score
            int[] Score = GameSession.CurrentMap.GetScore();
            if (Score[0] > Score[1]) {
                GameSession.BlueMapWins++;
            }
            else if (Score[1] > Score[0]) {
                GameSession.OrangeMapWins++;
            }

            //Update OBS Map Score
            if (Settings.Default.UseOBSDynamicText) {
                OBSEd.SetContent(OBSDynamicTextEditor.FileName.ScoreBlue, GameSession.BlueMapWins.ToString());
                OBSEd.SetContent(OBSDynamicTextEditor.FileName.ScoreOrange, GameSession.OrangeMapWins.ToString());
            }

            //Cancel if Current Map has no Round Data
            if (GameSession.CurrentMap.GetRoundCount() == 0) {
                UpdateStatus(StatusMessages.CurrentMapNoRounds, true);
                return;
            }

            if (!GameSession.NextMap()) {
                UpdateStatus(StatusMessages.NoMoreMaps);
                return;
            }

            //Update OBS Next Map Text
            if (GameSession.CurrentMap.GetMapIndex() == 2) {
                OBSEd.SetContent(OBSDynamicTextEditor.FileName.NextMap,
                    string.Format(main.NextMapPrefix, "3") + GameSession.CurrentMap.GetMapName());
            }

            //(Re-)Setup UI Elements
            ClearMapElements();

            //Reset BombSpots
            RoundBombSpotComboBox.Items.Clear();
            RoundBombSpotComboBox.Items.AddRange(GameSession.CurrentMap.GetBombSpots());

            UpdateStatus(StatusMessages.MapCompleted);

            //Select Operator Tab
            MainTabs.SelectTab(1);
            BanConfirmButton.Text =
                string.Format(main.BanConfirmButtonText, "\"" + GameSession.CurrentMap.GetMapName() + "\"");
        }

        #endregion
    }
}